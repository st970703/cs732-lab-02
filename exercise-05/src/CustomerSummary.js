import { Typography } from '@material-ui/core';
import { membershipTiers } from './utils/data-sort-utils';
import { connect } from 'react-redux';



function CustomerSummary({customers}) {
    function countTiers(customers, tier) {
        return customers.filter(
            customer => customer.membershipTier === tier
        ).length;
    }
    
    function getCustomerDetails(customers, tierNames) {
        let result = {};
    
        for (let i = 0; i < tierNames.length; i++) {
            result[tierNames[i]] = countTiers(customers, tierNames[i]);
        }
    
        return result;
    }

    const tierNames = Object.keys(membershipTiers);

    const details = getCustomerDetails(customers, tierNames);

    const sumValues = obj => Object.values(obj).reduce((a, b) => a + b);
    const customerSum = sumValues(details);

    return (
        <Typography>
            Customer details:&#160;
            {Object.keys(details).map(tier => (
                <span
                    key={tier}
                    value={tier}>
                    {tier}: {details[tier]},&#160;
                </span>
            ))}
            <strong>Total: {customerSum}</strong>
        </Typography>
    );
}

function mapStateToProps(state) {
    return {
        customers: state.customers
    };
}

// Applies the config using the "connect" higher-order component provided by Redux
export default connect(mapStateToProps)(CustomerSummary);