import Modal from './Modal';
import { useContext, useState } from 'react';
import styles from './NewCustomerDialog.module.css';
import { AppContext } from './AppContextProvider';
import { useHistory } from 'react-router-dom';
import { MEMBERSHIP_TIERS, TIME_UNITS } from './data';
import dayjs from 'dayjs';


export default function NewCustomerDialog() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [dob, setDob] = useState('');
    const [membershipExpiryLength, setExpiryLength] = useState(0);
    const [membershipExpiryUnit, setExpiryUnit] = useState(TIME_UNITS[0]);
    const [membershipTier, setTier] = useState(MEMBERSHIP_TIERS[0]);

    const { addCustomer } = useContext(AppContext);

    const history = useHistory();

    function handleAddCustomer() {
        if (firstName
            && lastName
            && dob
            && membershipExpiryLength
            && membershipExpiryUnit
            && membershipTier) {

            const membershipExpires = dayjs().add(parseInt(membershipExpiryLength), membershipExpiryUnit);          
            addCustomer({ firstName, lastName, dob, membershipExpires, membershipTier });

            history.replace(`/customers`);
        }
    }

    // Handles when we should cancel this dialog.
    function handleCancelNewCustomer() {
        history.goBack();
    }

    return (
        <Modal style={{ width: '50%', height: 'auto' }} dismissOnClickOutside={true} onCancel={handleCancelNewCustomer}>
            <h2>Add customer</h2>
            <div className={styles.form}>
                <div className={styles.formRow}>
                    <label>First Name:</label>
                    <input type="text"
                        value={firstName}
                        onInput={e => setFirstName(e.target.value)}
                        required
                    />
                </div>
                <div className={styles.formRow}>
                    <label>Last Name:</label>
                    <input type="text"
                        value={lastName}
                        onInput={e => setLastName(e.target.value)}
                        required
                    />
                </div>
                <div className={styles.formRow}>
                    <label>DOB:</label>
                    <input type="date"
                        value={dob}
                        onInput={e => setDob(e.target.value)}
                        required
                    />
                </div>
                <div className={styles.formRow}>
                    <label>Membership Expiry:</label>
                    <input type="number"
                        value={membershipExpiryLength}
                        onInput={e => setExpiryLength(e.target.value)}
                        required
                    />
                    <select value={membershipExpiryUnit}
                        onChange={e => setExpiryUnit(e.target.value)}
                        required >
                        {TIME_UNITS.map(TIME_UNIT => (
                            <option
                                key={TIME_UNIT}
                                value={TIME_UNIT}>
                                {TIME_UNIT}
                            </option>
                        ))}
                    </select>
                </div>
                <div className={styles.formRow}>
                    <label>Tier:</label>

                    <select value={membershipTier}
                        onChange={e => setTier(e.target.value)}
                        required >
                        {MEMBERSHIP_TIERS.map(MEMBERSHIP_TIER => (
                            <option
                                key={MEMBERSHIP_TIER}
                                value={MEMBERSHIP_TIER}>
                                {MEMBERSHIP_TIER}
                            </option>
                        ))}
                    </select>
                </div>             

                <div className={styles.formRow} style={{ flexDirection: 'row-reverse' }}>
                    <button
                        style={{ flexBasis: '100px', flexGrow: 0 }}
                        onClick={handleCancelNewCustomer}>
                        Cancel
                    </button>

                    <button
                        style={{ flexBasis: '100px', flexGrow: 0 }}
                        onClick={handleAddCustomer}>
                        Add
                    </button>
                </div>
            </div>
        </Modal>
    );
}