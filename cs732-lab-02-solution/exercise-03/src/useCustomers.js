import createPersistedState from 'use-persisted-state';
import { initialCustomers } from './data';

const usePersistedCustomers = createPersistedState('customers');

/**
 * Allows customers to be saved in localStorage, and exports the standard customers list and setCustomers
 * function, along with "add", "update", and "delete" convenience functions which are wrappers around setCustomers.
 */
export const useCustomers = () => {
    const [customers, setCustomers] = usePersistedCustomers(initialCustomers);

    const addCustomer = (customer) => setCustomers([...customers, customer]);

    const updateCustomer = (customer) => {
        const newCustomers = [...customers];
        for (let i = 0; i < newCustomers.length; i++) {
            if (newCustomers[i].id === customer.id) {
                newCustomers[i] = customer;
                break;
            }
        }
        setCustomers(newCustomers);
    }

    const deleteCustomer = (customer) => {
        setCustomers(customers.filter(c => c.id !== customer.id));
    }

    return { customers, setCustomers, addCustomer, updateCustomer, deleteCustomer };
}