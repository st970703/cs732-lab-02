import { useCustomers } from './useCustomers';

function countTiers(customers) {
    const tiers = {
        'Bronze': 0,
        'Silver': 0,
        'Gold': 0,
        'Platinum': 0
    };

    customers.forEach(c => tiers[c.membershipTier]++);

    return tiers;
}

export default function CustomerSummary({ style }) {

    const { customers } = useCustomers();
    const tiers = countTiers(customers);

    return (
        <div style={style}>
            Customer details: Bronze: {tiers.Bronze}, Silver: {tiers.Silver}, Gold: {tiers.Gold}, Platinum: {tiers.Platinum}. <strong>Total: </strong>{customers.length}
        </div>
    );
}