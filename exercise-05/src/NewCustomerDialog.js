import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { membershipTiers } from './utils/data-sort-utils';
import { Dialog, DialogContent, DialogTitle, TextField, DialogContentText } from '@material-ui/core';
import { KeyboardDatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { addCustomer } from './redux/actions';
import { connect } from 'react-redux';


const tierNames = Object.keys(membershipTiers);
const useStyles = makeStyles((theme) => ({
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));



function NewCustomerDialog({customers, addCustomer}) {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');

    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth();
    const day = date.getDate();
    const [dob, setDob] = useState(
        new Date(year - 18, month, day));

    const [membershipExpires, setExpiry] = useState(
        new Date(Date.now() + 7 * 24 * 60 * 60 * 1000));

    const [membershipTier, setTier] = useState(tierNames[0]);

    const classes = useStyles();
    const history = useHistory();

    // from https://stackoverflow.com/questions/3066586/get-string-in-yyyymmdd-format-from-js-date-object
    function formatDate(date) {
        const mm = date.getMonth() + 1; // getMonth() is zero-based
        const dd = date.getDate();

        const result = [date.getFullYear(),
        (mm > 9 ? '' : '0') + mm,
        (dd > 9 ? '' : '0') + dd
        ].join('-');

        return result;
    }


    function handleAddCustomer() {
        if (firstName
            && lastName
            && dob
            && membershipExpires
            && membershipTier){
            addCustomer({ firstName, lastName, dob, membershipExpires, membershipTier });

            history.replace(`/customers`);
        }
    }

    // Handles when we should cancel this dialog.
    function handleCancelNewCustomer() {
        history.goBack();
    }

    return (
        <Dialog open={true} onClose={handleCancelNewCustomer} aria-labelledby="add-customer-dialog-title">
            <DialogTitle id="add-customer-dialog-title">Add customer</DialogTitle>
            <DialogContent>
                <TextField
                    autoFocus
                    margin="dense"
                    id="new-first-name"
                    label="First name"
                    type="text"
                    fullWidth
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                />
                <TextField
                    autoFocus
                    margin="dense"
                    id="new-last-name"
                    label="Last name"
                    type="text"
                    fullWidth
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                />
                <DialogContentText margin="dense">
                    Date of birth
                </DialogContentText>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                        format="yyyy-MM-dd"
                        fullWidth
                        disableFuture
                        id="dob"
                        value={dob}
                        onChange={e => setDob(formatDate(e))}
                        KeyboardButtonProps={{
                            "aria-label": "change date of birth"
                        }} />
                </MuiPickersUtilsProvider>
                <DialogContentText margin="dense">
                    Membership expiry
                </DialogContentText>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                    <KeyboardDatePicker
                        format="yyyy-MM-dd"
                        fullWidth
                        id="membershipExpires"
                        value={membershipExpires}
                        onChange={e => setExpiry(formatDate(e))}
                        disablePast
                        KeyboardButtonProps={{
                            "aria-label": "change membership expiry"
                        }} />
                </MuiPickersUtilsProvider>

                <FormControl className={classes.formControl}>
                    <InputLabel id="membership-tier-label">Tier</InputLabel>
                    <Select
                        labelId="membership-tier-label"
                        id="membership-tier"
                        value={membershipTier}
                        onChange={e => setTier(e.target.value)}>
                        {tierNames.map(tierName =>
                            <MenuItem key={tierName} value={tierName}>
                                {tierName}</MenuItem>
                        )}
                    </Select>
                </FormControl>

                <div>
                    <Grid container justify="flex-end">
                        <Button variant="contained"
                            color="secondary"
                            onClick={handleCancelNewCustomer}>
                            Cancel
                    </Button>

                        <Button variant="contained"
                            color="primary"
                            onClick={handleAddCustomer}>
                            Add
                    </Button>
                    </Grid>
                </div>

            </DialogContent>
        </Dialog>
    );
}

function mapStateToProps(state) {
    return {
        customers: state.customers
    };
}

const mapDispatchToProps = {
    addCustomer
}

export default connect(mapStateToProps, mapDispatchToProps)(NewCustomerDialog);