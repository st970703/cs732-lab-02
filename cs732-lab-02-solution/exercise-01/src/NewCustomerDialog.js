import Modal from './Modal';
import { useState } from 'react';
import styles from './NewCustomerDialog.module.css';
import dayjs from 'dayjs';

/**
 * Exercise Two: New Customer dialog box.
 */
export default function NewCustomerDialog({ onAddCustomer, onCancelAddCustomer }) {

    const [customer, setCustomer] = useState({
        id: 0,
        firstName: '',
        lastName: '',
        dob: dayjs(),
        membershipExpires: dayjs(),
        membershipTier: 'Bronze'
    });

    function modifyCustomer(fields) {
        setCustomer({ ...customer, ...fields });
    }

    return (
        <Modal style={{ width: '60%', height: 'auto' }} dismissOnClickOutside={true} onCancel={onCancelAddCustomer}>
            <h2>Add customer</h2>
            <div className={styles.form}>
                <div className={styles.formRow}>
                    <label>ID:</label>
                    <input type="number" value={customer.id} onInput={e => modifyCustomer({ id: parseInt(e.target.value) })} />
                </div>
                <div className={styles.formRow}>
                    <label>First name:</label>
                    <input type="text" value={customer.firstName} onInput={e => modifyCustomer({ firstName: e.target.value })} />
                </div>
                <div className={styles.formRow}>
                    <label>Last name:</label>
                    <input type="text" value={customer.lastName} onInput={e => modifyCustomer({ lastName: e.target.value })} />
                </div>
                <div className={styles.formRow}>
                    <label>Date of birth:</label>
                    <input type="date" value={dayjs(customer.dob).format('YYYY-MM-DD')} onInput={e => modifyCustomer({ dob: dayjs(e.target.value) })} />
                </div>
                <div className={styles.formRow}>
                    <label>Membership tier:</label>
                    <select value={customer.membershipTier} onChange={e => modifyCustomer({ membershipTier: e.target.value })}>
                        <option value="Bronze">Bronze</option>
                        <option value="Silver">Silver</option>
                        <option value="Gold">Gold</option>
                        <option value="Platinum">Platinum</option>
                    </select>
                </div>
                <div className={styles.formRow}>
                    <label>Membership expiry:</label>
                    <input type="date" value={dayjs(customer.membershipExpires).format('YYYY-MM-DD')} onInput={e => modifyCustomer({ membershipExpires: dayjs(e.target.value) })} />
                </div>

                <div className={styles.formRow} style={{ flexDirection: 'row-reverse' }}>
                    <button
                        style={{ flexBasis: '100px', flexGrow: 0 }}
                        onClick={() => onAddCustomer(customer)}>
                        Add
                    </button>
                </div>
            </div>
        </Modal>
    );
}