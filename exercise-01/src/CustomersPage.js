import styles from './CustomersPage.module.css';
import CustomerTable from './CustomerTable';
import { AppContext } from './AppContextProvider';
import { Link, useRouteMatch } from 'react-router-dom';
import { useContext } from 'react';

export default function CustomersPage() {

    const { customers } = useContext(AppContext);
    const { url } = useRouteMatch();

    return (
        <>
            <main>
                <div className="box">
                    <CustomerTable customers={customers} />
                </div>

                <div className={styles.formRow} style={{ flexDirection: 'row-reverse' }} >
                    <Link to={`${url}/add`}>
                        <button
                             style={{ flexBasis: '100px', flexGrow: 0 }}>
                            Add Customer
                    </button>
                    </Link>
                </div>
            </main>
        </>
    )
}