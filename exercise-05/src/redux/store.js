import { createStore } from 'redux';
import rootReducer from './reducers';
import { addCustomer } from './actions';
import { composeWithDevTools } from 'redux-devtools-extension';
import { initialCustomers } from '../utils/data';
import { loadState, saveState } from '../utils/localStorage';
import throttle from 'lodash.throttle';

const persistedState = loadState();

const store = createStore(rootReducer,
    persistedState,
    composeWithDevTools());

// Add data...
if(!persistedState) {
    initialCustomers.forEach(customer => store.dispatch(addCustomer(customer)));
}

store.subscribe(throttle(() => {
    saveState({
        customers: store.getState().customers
    });
}, 1000));

export default store;