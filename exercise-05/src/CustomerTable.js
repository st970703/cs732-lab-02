import dayjs from 'dayjs';
import { sortCustomers, isActive } from './utils/data-sort-utils';
import createPersistedState from 'use-persisted-state';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { TABLE_COLUMNS, TABLE_COLUMN_CATEGORIES } from './utils/data';
import TableSortLabel from '@material-ui/core/TableSortLabel';


const useSortProperty = createPersistedState('sortProperty');
const useSortDirection = createPersistedState('sortDirection');

const useStyles = makeStyles((theme) => ({
    table: {
        minWidth: 650,
    },

    active: {
        color: 'inherit',
    },
    
    inactive: {
        color: 'red',
    },
}));

export default function CustomerTable({ customers }) {

    const [sortProperty, setSortProperty] = useSortProperty('none');
    const [sortDirection, setSortDirection] = useSortDirection('none');

    const handleSortTableClick = (newSortProperty) => {
        if (sortDirection === 'asc') {
            setSortDirection('desc');
            setSortProperty(newSortProperty);
        } else if (sortDirection === 'desc') {
            setSortDirection('none');
            setSortProperty('none');
        } else {
            setSortDirection('asc');
            setSortProperty(newSortProperty);
        }
    }

    const sortedCustomers = sortCustomers(customers, sortProperty, sortDirection);

    const classes = useStyles();

    return (
        <div style={{ paddingTop: '60px' }}>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="customer table">
                    <TableHead>
                        <TableRow>
                            {TABLE_COLUMN_CATEGORIES.map(columnCategory => (
                                <TableCell colSpan={4}
                                 align="center"
                                key={columnCategory}>
                                    {columnCategory}
                                </TableCell>
                            ))}
                        </TableRow>
                        <TableRow>
                            {TABLE_COLUMNS.map(tableColumn => (
                                <TableCell key={tableColumn.id}>
                                <TableSortLabel
                                key={tableColumn.id}
                                active={sortProperty === tableColumn.id}
                                direction={sortProperty === tableColumn.id ? sortDirection : 'asc'}
                                onClick={e => handleSortTableClick(tableColumn.id)}>
                                    {tableColumn.label}
                                    </TableSortLabel>
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {sortedCustomers.map(sortedCustomer => (
                            <TableRow key={sortedCustomer.id}>
                                <TableCell component="th"
                                scope="row"
                                key="id">
                                    {sortedCustomer.id}
                                </TableCell>
                                <TableCell key="firstName">
                                {sortedCustomer.firstName}</TableCell>
                                <TableCell key="lastName">
                                {sortedCustomer.lastName}</TableCell>
                                <TableCell key="dob">
                                {dayjs(sortedCustomer.dob).format('ll')}</TableCell>
                                <TableCell key="isActive"
                                className={isActive(sortedCustomer) ? classes.active: classes.inactive}>
                                {isActive(sortedCustomer) ? 'Yes' : 'No'}</TableCell>
                                <TableCell key="membershipTier">
                                {sortedCustomer.membershipTier}</TableCell>
                                <TableCell key="membershipExpires">
                                {dayjs(sortedCustomer.membershipExpires).calendar()}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    );
}