import { ADD_CUSTOMER } from '../action-types';
import dayjs from 'dayjs';

export default function customers(state = [], action) {
    // Perform different things based on the type of action
    switch (action.type) {

        // To add a to-do item, we create a new array containing all elements in the current array,
        // with the new to-do item added onto the end.
        case ADD_CUSTOMER:
            const newCustomer = {
                id: state.length + 1,
                firstName: action.customer.firstName,
                lastName: action.customer.lastName,
                dob: dayjs(action.customer.dob),
                membershipExpires: dayjs(action.customer.membershipExpires),
                membershipTier: action.customer.membershipTier,
            };


            return [
                ...state, /* This syntax means "All items in the state array".
                             It essentially adds all those items to this new array that's being created. */
                newCustomer
            ]

        default:
            return state;
    }
}