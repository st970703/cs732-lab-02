import CustomerTable from './CustomerTable';
import { AppContext } from './AppContextProvider';
import { useContext } from 'react';
import { useHistory } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';


export default function CustomersPage() {

    const { customers } = useContext(AppContext);
    const history = useHistory();


    return (
        <>
            <main>
                <div>
                    <CustomerTable customers={customers} />
                </div>

                <div>
                    <Grid container justify="flex-end">
                        <Button variant="contained"
                            onClick={e => history.push(`/customers/add`)}>
                            Add
                    </Button>
                    </Grid>
                </div>
            </main>
        </>
    )
}