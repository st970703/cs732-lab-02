import styles from './NavBar.module.css';
import { NavLink } from 'react-router-dom';
import { AppContext } from './AppContextProvider';
import { useContext } from 'react';


export default function NavBar() {
    const { getCustomerDetails } = useContext(AppContext);

    const details = getCustomerDetails();
    const sumValues = obj => Object.values(obj).reduce((a, b) => a + b);
    const customerSum = sumValues(details);

    return (
        <div>
            <div className={styles.navBar}>
                <NavLink to="/customers" activeClassName={styles.activeLink}>Customers</NavLink>
                </div>
                <div>
                Customer details:&#160;
            {Object.keys(details).map(tier => (
                            <span
                                key={tier}
                                value={tier}>
                                {tier}: {details[tier]},&#160; 
                            </span>
                        ))}
        
                <strong>Total: {customerSum}</strong>
            </div>
        </div>

    );
}