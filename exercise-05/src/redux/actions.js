import { ADD_CUSTOMER } from './action-types';

export function addCustomer(customer) {
    return {
        type: ADD_CUSTOMER,
        customer
    }
}
