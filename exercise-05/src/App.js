import { Switch, Route, Redirect, } from 'react-router-dom';
import NavBar from './NavBar';
import CustomersPage from './CustomersPage';
import NewCustomerDialog from './NewCustomerDialog';
import { connect } from 'react-redux';



/**
 * Renders a navbar allowing the user to browse to the articles or gallery pages.
 * If the user tries to browse to any other URL, they are auto-redirected to the articles page.
 */
function App() {

  return (
    <div>

      <nav>
        <NavBar />
      </nav>

      <Switch>
        <Route path="/customers">
          <CustomersPage />
        </Route>

        <Route path="*">
          <Redirect to="/customers" />
        </Route>
      </Switch>

      <Switch>
        <Route path={`/customers/add`}>
          <NewCustomerDialog />
        </Route>
      </Switch>

    </div>
  );
}

// export default App;

function mapStateToProps(state) {
  return {
    customers: state.customers
  };
}

export default connect(mapStateToProps)(App);