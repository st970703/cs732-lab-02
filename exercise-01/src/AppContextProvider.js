import React from 'react';
import createPersistedState from 'use-persisted-state';
import { initialCustomers, MEMBERSHIP_TIERS } from './data';
import dayjs from 'dayjs';

// Create the context
const AppContext = React.createContext({
    customers: initialCustomers
});

const useCustomersState = createPersistedState('customers');

function AppContextProvider({ children }) {

    // Stateful value initialization
    const [customers, setCustomers] = useCustomersState(initialCustomers);

    function addCustomer({ firstName, lastName, dob, membershipExpires, membershipTier }) {
        const newCustomer = {
            id: customers.length + 1,
            firstName,
            lastName,
            dob: dayjs(JSON.stringify(dob)),
            membershipExpires,
            membershipTier
        };

        setCustomers([
            ...customers,
            newCustomer
        ]);

        return newCustomer;
    }

    function countTiers(tier) {
        return customers.filter(
            customer => customer.membershipTier === tier
        ).length;
    }

    function getCustomerDetails() {
        let result = {};

        for (let i = 0; i < MEMBERSHIP_TIERS.length; i++) {
            result[MEMBERSHIP_TIERS[i]] = countTiers(MEMBERSHIP_TIERS[i]);
        }

        return result;
    }

    // The context value that will be supplied to any descendants of this component.
    const context = {
        customers,
        addCustomer,
        getCustomerDetails
    }

    // Wraps the given child components in a Provider for the above context.
    return (
        <AppContext.Provider value={context}>
            {children}
        </AppContext.Provider>
    );
}

export {
    AppContext,
    AppContextProvider
};