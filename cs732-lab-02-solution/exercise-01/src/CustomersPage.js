import CustomerTable from './CustomerTable';
import { useState } from 'react';
import { initialCustomers } from './data';
import { useHistory, Switch, Route } from 'react-router-dom';
import NewCustomerDialog from './NewCustomerDialog';

export default function CustomersPage() {

    const history = useHistory();
    const [customers, setCustomers] = useState(initialCustomers);

    return (
        <>
            <main>
                <div className="box">
                    <CustomerTable customers={customers} />

                    {/* Exercise Two: "Add customer" button. */}
                    <button onClick={() => history.push('/customers/add')}>New customer</button>
                </div>

                {/* Exercise Two: "Add customer" dialog box. */}
                <Switch>
                    <Route path="/customers/add">
                        <NewCustomerDialog
                            onAddCustomer={customer => {
                                setCustomers([...customers, customer])
                                history.goBack();
                            }}
                            onCancelAddCustomer={() => history.goBack()} />
                    </Route>
                </Switch>
            </main>
        </>
    )
}