import CustomerTable from './CustomerTable';
import { useHistory, Switch, Route } from 'react-router-dom';
import NewCustomerDialog from './NewCustomerDialog';
import { useCustomers } from './useCustomers';

export default function CustomersPage() {

    const history = useHistory();
    const { customers, addCustomer } = useCustomers();

    return (
        <>
            <main>
                <div className="box">
                    <CustomerTable customers={customers} />

                    {/* Exercise Two: "Add customer" button. */}
                    <button onClick={() => history.push('/customers/add')}>New customer</button>
                </div>

                {/* Exercise Two: "Add customer" dialog box. */}
                <Switch>
                    <Route path="/customers/add">
                        <NewCustomerDialog
                            onAddCustomer={customer => {
                                addCustomer(customer);
                                history.goBack();
                            }}
                            onCancelAddCustomer={() => history.goBack()} />
                    </Route>
                </Switch>
            </main>
        </>
    )
}