import CustomerTable from './CustomerTable';
import { useHistory } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { connect } from 'react-redux';


function CustomersPage({customers}) {
    const history = useHistory();

    return (
        <>
            <main>
                <div>
                    <CustomerTable customers={customers} />
                </div>

                <div>
                    <Grid container justify="flex-end">
                        <Button variant="contained"
                            onClick={e => history.push(`/customers/add`)}>
                            Add
                    </Button>
                    </Grid>
                </div>
            </main>
        </>
    )
}

function mapStateToProps(state) {
    return {
        customers: state.customers
    };
}

// Applies the config using the "connect" higher-order component provided by Redux
export default connect(mapStateToProps)(CustomersPage);