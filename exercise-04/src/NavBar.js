import { Link } from 'react-router-dom';

import { AppBar, makeStyles, Tab, Tabs, Toolbar } from '@material-ui/core';
import { useLocation } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';
import CustomerSummary from './CustomerSummary';


const navbarTabs = [
    { title: 'Customers', path: '/customers' },
];

function useTabIndex() {
    const { pathname } = useLocation();
    for (let i = 0; i < navbarTabs.length; i++) {
        if (pathname.startsWith(navbarTabs[i].path))
            return i;
    }
    return 0;
}

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    }
}));

export default function NavBar() {




    const classes = useStyles();
    const tabIndex = useTabIndex();

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Grid container
                    justify="space-between"
                    alignItems="center">
                        <Grid item>
                            <Tabs value={tabIndex} aria-label="main navigation tabs">
                                {navbarTabs.map((tab, index) => (
                                    <Tab key={index} label={tab.title} component={Link} to={tab.path} />
                                ))}
                            </Tabs>
                        </Grid>
                        <Grid item>
                            <CustomerSummary />
                        </Grid>
                    </Grid>
                </Toolbar>
            </AppBar>
        </div>
    );
}