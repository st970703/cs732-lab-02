import styles from './NavBar.module.css';
import { NavLink } from 'react-router-dom';
import CustomerSummary from './CustomerSummary';

export default function NavBar() {
    return (
        <div className={styles.navBar}>
            <NavLink to="/customers" activeClassName={styles.activeLink}>Customers</NavLink>

            {/* Exercise Three: Added customersummery component */}
            <CustomerSummary style={{ alignSelf: 'center', marginLeft: 20 }} />
        </div>
    );
}