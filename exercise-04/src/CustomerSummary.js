import { AppContext } from './AppContextProvider';
import { useContext } from 'react';
import { Typography } from '@material-ui/core';


export default function CustomerSummary() {
    const { getCustomerDetails } = useContext(AppContext);
    const details = getCustomerDetails();
    const sumValues = obj => Object.values(obj).reduce((a, b) => a + b);
    const customerSum = sumValues(details);

    return (
        <Typography>
            Customer details:&#160;
            {Object.keys(details).map(tier => (
                <span
                    key={tier}
                    value={tier}>
                    {tier}: {details[tier]},&#160;
                </span>
            ))}
            <strong>Total: {customerSum}</strong>
        </Typography>
    );
}