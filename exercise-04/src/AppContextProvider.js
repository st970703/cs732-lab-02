import React from 'react';
import createPersistedState from 'use-persisted-state';
import { initialCustomers } from './data';
import { membershipTiers } from './data-sort-utils';
import dayjs from 'dayjs';

const tierNames = Object.keys(membershipTiers);

// Create the context
const AppContext = React.createContext({
    customers: initialCustomers
});

const useCustomersState = createPersistedState('customers');

function AppContextProvider({ children }) {

    // Stateful value initialization
    const [customers, setCustomers] = useCustomersState(initialCustomers);

    function addCustomer({ firstName, lastName, dob, membershipExpires, membershipTier }) {
        const newCustomer = {
            id: customers.length + 1,
            firstName,
            lastName,
            dob: dayjs(dob),
            membershipExpires: dayjs(membershipExpires),
            membershipTier
        };

        setCustomers([
            ...customers,
            newCustomer
        ]);

        return newCustomer;
    }

    function countTiers(tier) {
        return customers.filter(
            customer => customer.membershipTier === tier
        ).length;
    }

    function getCustomerDetails() {
        let result = {};

        for (let i = 0; i < tierNames.length; i++) {
            result[tierNames[i]] = countTiers(tierNames[i]);
        }

        return result;
    }

    // The context value that will be supplied to any descendants of this component.
    const context = {
        customers,
        addCustomer,
        getCustomerDetails
    }

    // Wraps the given child components in a Provider for the above context.
    return (
        <AppContext.Provider value={context}>
            {children}
        </AppContext.Provider>
    );
}

export {
    AppContext,
    AppContextProvider
};